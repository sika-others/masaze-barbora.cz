<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
<meta http-equiv="content-type" content="<?php bloginfo('html_type') ?>; charset=<?php bloginfo('charset') ?>" />
<title>
<?php if (function_exists('is_tag') && is_tag()) {
single_tag_title('Tag Archive for &quot;'); echo '&quot; - ';
} elseif (is_archive()) {
wp_title(''); echo ' Archive - ';
} elseif (is_search()) {
echo 'Search for &quot;'.esc_html($s).'&quot; - ';
} elseif (!(is_404()) && (is_single()) || (is_page())) {
wp_title(''); echo ' - ';
} elseif (is_404()) {
echo 'Not Found - ';
}
if (is_home()) {
bloginfo('name'); echo ' - '; bloginfo('description');
} else {
bloginfo('name');
}
if ($paged > 1) {
echo ' - page '. $paged;
} ?>
</title>
<?php if(is_search()) { ?>
	<meta name="robots" content="noindex, nofollow" /> 
<?php }?>
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
				
<!-- Start Open Web Analytics Tracker -->
<script type="text/javascript">
//<![CDATA[
var owa_baseUrl = 'http://owa.ondrejsika.com/';
var owa_cmds = owa_cmds || [];
owa_cmds.push(['setSiteId', '499f7f7ef6ec83a5b1ec36639dd9212d']);
owa_cmds.push(['trackPageView']);
owa_cmds.push(['trackClicks']);
owa_cmds.push(['trackDomStream']);

(function() {
	var _owa = document.createElement('script'); _owa.type = 'text/javascript'; _owa.async = true;
	owa_baseUrl = ('https:' == document.location.protocol ? window.owa_baseSecUrl || owa_baseUrl.replace(/http:/, 'https:') : owa_baseUrl );
	_owa.src = owa_baseUrl + 'modules/base/js/owa.tracker-combined-min.js';
	var _owa_s = document.getElementsByTagName('script')[0]; _owa_s.parentNode.insertBefore(_owa, _owa_s);
}());
//]]>
</script>
<!-- End Open Web Analytics Code -->
				
		
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/custom.css'; ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
wp_enqueue_script('superfish-init', get_template_directory_uri() . '/js/superfish-init.js');

$options = get_option('tpSunrise_options');
if ( 'blank' == get_header_textcolor() ) { ?>
	  <style type="text/css">
	  #header_text h1 a {display: none;}
	  #header_text .description {display: none;}
	  </style>
<?php } else { ?>  
	  <style type="text/css">
	  #header_text h1 a {color: #<?php echo get_header_textcolor() ?>;}
	  #header_text .description {color: #<?php echo get_header_textcolor() ?>;}
	  </style>
<?php }

// get alternate styling for home page
if( (is_front_page()) ) {  // for home page only
	  if ($options['use_alt_home_style']) { ?>
		    <!-- alternate home page styling -->
		    <style type="text/css">
		    <?php echo $options['alt_home_style_text']; ?>
		    </style>

	  <?php }
}
	
wp_head(); ?>
   
</head>

<body <?php body_class(); ?>>

<div id="masthead">
			    <h1><a href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a></h1>
			    <h2 class="description"><?php bloginfo( 'description' ); ?></h2>
</div>
