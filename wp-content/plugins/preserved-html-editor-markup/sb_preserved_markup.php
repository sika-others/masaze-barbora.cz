<?php

/*
Plugin Name: Preserved HTML Editor Markup
Plugin URI: http://www.marcuspope.com/wordpress/
Description: A Wordpress Plugin that preserves HTML markup in the TinyMCE editor, especially when switching between
html and visual tabs.  Also adds support for HTML5 Block Anchors.
Author: Marcus E. Pope, marcuspope
Author URI: http://www.marcuspope.com
Version: 1.0

Copyright 2011 Marcus E. Pope (email : me@marcuspope.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

add_action('plugins_loaded', array('MP_WP_Preserved_Markup', 'init'), 1);

class MP_WP_Preserved_Markup {

    public static function remove_evil() {
        //remove evil: wpautop will break html5 markup!
        remove_filter('the_content', 'wpautop');
    }

    public static function init_tiny_mce($init) {
        //Setup tinymce editor with necessary settings for better general editing
        $tags = "pre[*],iframe[*],object[*],param[*]"; //add pre and iframe to allowable tags for
        if (isset($init['extended_valid_elements'])) {
            $tags = $init['extended_valid_elements'] . "," . $tags;
        }
        $init['extended_valid_elements'] = $tags;
        $init['forced_root_block'] = false; //prevent tinymce from wrapping a root block level element (typically a <p> tag)
        $init['force_p_newlines'] = false;
        $init['remove_linebreaks'] = false;
        $init['force_br_newlines'] = true;
        $init['remove_trailing_nbsp'] = false;
        $init['relative_urls'] = true;
        $init['convert_urls'] = false;
        $init['remove_linebreaks'] = false;
        $init['doctype'] = '<!DOCTYPE html>';
        $init['apply_source_formatting'] = false;
        $init['convert_newlines_to_brs'] = false;
        $init['fix_list_elements'] = false;
        $init['fix_table_elements'] = false;
        $init['verify_html'] = false;

        /*
           Allow for html5 anchor tags
           http://dev.w3.org/html5/markup/a.html
           http://dev.w3.org/html5/markup/common-models.html#common.elem.phrasing
           http://www.tinymce.com/wiki.php/Configuration:valid_children
        */
        $init['valid_children'] = "+a[em|strong|small|mark|abbr|dfn|i|b|s|u|code|var|samp|kbd|sup|sub|q|cite|span|bdo|bdi|br|wbr|ins|del|img|embed|object|iframe|map|area|script|noscript|ruby|video|audio|input|textarea|select|button|label|output|datalist|keygen|progress|command|canvas|time|meter|p|hr|pre|ul|ol|dl|div|h1|h2|h3|h4|h5|h6|hgroup|address|blockquote|section|nav|article|aside|header|footer|figure|table|f|m|fieldset|menu|details]";

        return $init;
    }

    public static function fix_editor_content($html) {
        remove_filter('the_editor_content', "wp_richedit_pre");
        return $html;
    }

    public static function content_replace_callback($a) {
        $s = $a[0];
        $s = preg_replace("/(\r\n|\n)/m", '<mep-preserve-nl>', $s);
        $s = preg_replace("/\t/m", '<mep-preserve-tab>', $s);
        $s = preg_replace("/\s/m", '<mep-preserve-space>', $s);
        return $s;
    }

    public static function fix_wysiwyg_content($c) {
        //If the page is rendered with the WYSIWYG editor selected by default, content is processed in PHP land
        //instead of using the JS land "equivalent" logic (I quote equivalent because there are sooooo many
        //discrepancies between what JS wpautop and PHP wpautop functions do it's laughable.
        if (wp_default_editor() == "tinymce") {
            //First we inject temporary whitespace markers in pre and code elements because they won't
            //be corrupted when the user switches to html mode.*   (actually IE9 will remove the first
            //newline from a pre tag if there are no non-whitespace characters before the newline.)
            $c = preg_replace_callback(
                '/<(pre|code)[^>]*>[\s\S]+?<\/\\1>/m',
                array(
                    'MP_WP_Preserved_Markup',
                    'content_replace_callback'
                ),
                $c);

            //Now let's preserve whitespace with html comments so that they can be converted back when switching to
            //the html mode.  FIXME: assuming four spaces is bad mmkay, what if I like only two spaces for a tab?
            //and this could produce bad markup if a user had <p    class="test">hello</p> in their markup.  So
            //work on a more flexible /\s/g approach when \s is inside or outside a tag definition
            $c = preg_replace("/(\r\n|\n)/", "<!--mep-nl-->", $c); //preserve new lines
            $c = preg_replace("/(\t|\s\s\s\s)/", "<!--mep-tab-->", $c); //preserve indents

            //Now we can restore all whitespace originally escaped in pre & code tags
            $c = preg_replace("/<mep-preserve-nl>/m", "\n", $c);
            $c = preg_replace("/<mep-preserve-tab>/m", "\t", $c);
            $c = preg_replace("/<mep-preserve-space>/m", " ", $c);

            //finish up with functions that WP normally calls on the_editor_content
            $c = convert_chars($c);
            $c = htmlspecialchars($c, ENT_NOQUOTES);
        }

        return $c;
    }

    public static function fix_post_content($post) {
        //If the user clicks save while in the Visual (WYSIWYG) tab, we'll need to strip the whitespace placeholders
        //before inserting the data into the database to prevent duplication of whitespace
        //WTF: ok so I ran into a problem of duplicating newlines when saving from the Visual tab, so I added this
        //     code to strip what I thought was my whitespace markers not being converted back in JS land before being
        //     sent to the server.  (in the same way that if you load the page on the Visual tab, the parsing logic
        //     occurs on the server instead of the client!)  But after add this code I couldn't reproduce the issue
        //     the post_content never contained any mep-xxx comments.  I'm leaving this code in here, because it's
        //     the probable explanation for duplicated newlines, but I'm also not sure how the Visual content is
        //     transfered from the iframe to the textarea.
        if (isset($post['post_content'])) {
            $post['post_content'] = preg_replace('/<\!--mep-nl-->/m', "\r\n", $post['post_content']);
            $post['post_content'] = preg_replace('/<\!--mep-tab-->/m', "    ", $post['post_content']);
        }
        return $post;
    }

    public static function admin_init() {
        //Add full attribute support for special tags
        add_filter( 'tiny_mce_before_init', array(
            'MP_WP_Preserved_Markup',
            'init_tiny_mce'
        ));

        /* fix WP html editor on client side */
        wp_enqueue_script('admin-js', WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__))."admin.js");

        add_filter('the_editor', array(
            'MP_WP_Preserved_Markup',
            'fix_editor_content'
        ), 1);

        add_filter('the_editor_content', array(
            'MP_WP_Preserved_Markup',
            'fix_wysiwyg_content'
        ), 1);

        add_filter('wp_insert_post_data', array(
            'MP_WP_Preserved_Markup',
            'fix_post_content'
        ), 1);
    }

    public static function init() {
        add_action('init', array(
            'MP_WP_Preserved_Markup',
            'remove_evil'
        ));

        add_action('admin_init', array(
            'MP_WP_Preserved_Markup',
            'admin_init'
        ));
    }
}