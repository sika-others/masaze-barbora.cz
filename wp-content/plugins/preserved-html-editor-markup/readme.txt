=== Preserved HTML Editor Markup ===
Contributors: marcuspope
Donate link: http://www.marcuspope.com/
Tags: wpautop, editor, markup, html, white space, HTML5, WYSIWYG, visual, developer
Requires at least: 3.2.1
Tested up to: 3.2.1
Stable tag: trunk

Preserves white space and developer edits in HTML tab AND WYSIWYG tab and adds support for HTML5 block anchor tags

== Description ==

This plugin preserves the user-generated HTML markup in the TinyMCE editor.  Unlike other plugins this one allows developers to work in the HTML tab AND end-users to work in the WYSIWYG Visual tab at the same time!  No longer will your HTML markup be completely munged into an unrecognizable form when you switch between those tabs.  And you don't have to hang your users/editors out to dry when you hand off the project with a disabled Visual tab.

It also supports HTML5 Block Anchor tags, something that is currently not supported in WP even via any existing plugins.

There are a couple caveats to editing in this mode, but most of them are understood by developers and are even repeatedly asked for in forums and on Wordpress dev lists.  They are as follows:

  1. wpautop is disabled.  And I mean really disabled, not just disabled when editing in HTML, or when viewing the content, but the TinyMCE editor will never wrap your content in a p tag again (unless you tell it to of course.)

  1. In Visual mode, the "enter key" will inject BR tags instead of newlines in the HTML source.  It will still appear correct in the WYSIWYG result.

  1. Orphaned text content will not be wrapped in any tag - so you will want to make sure your theme inherently wraps the post content in a div tag (or some other block element) for valid HTML markup.

  1. White space is preserved; newlines, spaces and tabs will remain in your markup for the most part.  For performance reasons, it will only preserve spaces if 4 spaces are used consecutively - i.e. an expanded tab in developer terms.  It will not preserve intra-tag white space like &lt;p&nbsp;&nbsp;&nbsp;&nbsp;&gt;.

There are a couple of bugs in the parsing logic that I'm aware of and I will work to iron them out over the coming weeks.

Known issues:

  1. If you do add 4 or more spaces inside of an element tag it will corrupt the markup and mangle the output.  But as this is intended for developer edits, this should be an extreme rarity given the habit is virtually non-existent in development communities.

  1. If you use two or three spaces on a line they will be reduced to one.

  1. PRE tags are not affected and behave as you would expect, however due to how browsers parse tags, the first newline in the content of a PRE tag will be wiped out unless it is padded with either another new line or multiple spaces.

  1. CODE tags are not preserving white space at all, and when wrapped around PRE tags they are being removed entirely. When PRE tags are wrapped around CODE tags they are not removed, however white space is removed.  I'm working to resolve this problem.

== Installation ==

1. Upload the plugin contents to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in Wordpress Admin
1. You're done!

== Frequently Asked Questions ==

= Why doesn't the visual editor wrap my text content in paragraph &lt;p&gt; tags? =

This is a side effect of disabling the poorly designed code formatter that is built into Wordpress.  However, it should be possible to add this feature at some point down the road.

== Upgrade Notice ==

1. No upgrade notices

== Screenshots ==

1. No screenshots

== Changelog ==

= 1.1 =
* Refactored for support of < php5.3 by replacing function references with static function array refs
= 1.0 =
* Initial creation of plugin

== Arbitrary section ==
