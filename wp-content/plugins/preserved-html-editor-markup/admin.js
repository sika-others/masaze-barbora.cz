(function($) {
    //on dom load
    $(function() {

        //disable evil... yeah that's right I said EVIL!
        if (window.switchEditors) {
            switchEditors._wp_Nop = function(e) { return e; };
            switchEditors._wp_Autop = function(e) { return e; };
        }

        if (window.edButtons) {
            //Emphasis and Strong have very special meanings, and they DO NOT mean bold and italics people!!!
            edButtons[0] = new edButton('ed_strong','b','<b>','</b>','b');
            edButtons[1] = new edButton('ed_em','i','<i>','</i>','i');
        }

        $('body').bind('afterPreWpautop', function(e, o) {
            //On Switch to HTML

            //Now we replace all those temporary html comments with spaces and newlines
            o.data = o.unfiltered.replace(/<\!--mep-nl-->/g, "\r\n").replace(/<\!--mep-tab-->/g, "    ");
        }).bind('afterWpautop', function(e, o) {
            //On Switch to Visual

            //first preserve newlines and whitespace in pre & code tags because the browser will not mess with those
            if ( o.unfiltered.indexOf('<pre') != -1 || o.unfiltered.indexOf('<code') != -1 ) {
                o.unfiltered = o.unfiltered.replace(/<(pre|code)[^>]*>[\s\S]+?<\/\1>/g, function(s) {
                    return s.replace(/(\r\n|\n)/g, '<mep-preserve-nl>').replace(/\t/g, "<mep-preserve-tab>").replace(/\s/g, "<mep-preserve-space>");
                });
            }

            //replace any newline characters with a custom mep html comment as a marker for where
            //newline chars should be added back in when we're done
            o.data = o.unfiltered.replace(/(\r\n|\n)/g, "<!--mep-nl-->").replace(/(\t|\s\s\s\s)/g, "<!--mep-tab-->");

            //and restore the whitespace back in pre & code tags
            o.data = o.data.replace(/<mep-preserve-nl>/g, "\n").replace(/<mep-preserve-tab>/g, "\t").replace(/<mep-preserve-space>/g, " ");
        });
    });
})(jQuery);