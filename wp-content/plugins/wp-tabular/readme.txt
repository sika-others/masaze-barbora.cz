=== WP Tabular - HTML Table Generator for WordPress ===
Contributors: Shaon
Donate link: http://wpeden.com
Tags: table, wordpress table, pricing table, price table, special offer, discount offer, offer, important, page, posts, post, widget, links, image, admin, sidebar, plugins, plugin, comments, must have plugins, html table, css table
Requires at least: 2.0.2
Tested up to: 3.3.1
 
 

WP Table plugin will help you to generate html table simply using GUI
   

== Description ==

WordPress Tabular Plugin is a very easy to Table Builder Plugin for WordPress. WordPress Tabular Plugin has Complete GUI for creating and sorting rows and columns without writing any html. Also WordPress Tabular Plugin packed with many table templates, where you can select any template to show a table at front-end. As well as regular short-code, It will provide you js embed and you can use the js code even in another site to embed the table. Tables are exportable in xml, html and csv format. 

`v1.1.0 comes with custom shortcode support, create your own shortcode to embed any icon easily`

== Installation ==

1. Upload `wp-tabular` to the `/wp-content/plugins/`  directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 1.1.1 =
* tinymce button issue fixed

= 1.1.0 =
* added textarea instead of inputbox at table admin
* added shortcode option
* added new templates for tables

= 1.0.0 =
* initial release

