<?php if( !is_front_page() ):?>
<h2 class="post-title"><?php if( !is_singular() ) :?><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute( array('before' => __('Permalink to ', 'zenlite'), 'after' => '') ); ?>"><?php endif;?><?php the_title();?><?php if( !is_singular() ) :?></a><?php endif;?></h2>
<?php endif;?>

<?php global $zenlite_options;
if( (! isset ($zenlite_options['pagetree']) || $zenlite_options['pagetree'] != 'no' ) && zenlite_page_tree($post) != '' ) :?>
<div class="pagetree">
<h3><?php _e('Pages in this section', 'zenlite');?></h3>
<ul><?php echo zenlite_page_tree($post);?></ul>
</div>
<?php endif;?>

<div class="postcontent">
<?php if( is_search() ) the_excerpt();
else the_content();?>

<?php
$args = array(
	'before' => '<div class="pagelist">' . __('Pages:', 'zenlite'),
	'after' => '</div>',
	'link_before' => '',
	'link_after' => '',
	'pagelink' => '%'
);
wp_link_pages($args);?>

<?php if( is_page_template('authors.php' ) ) :?>
<ul class="authors">
<?php wp_list_authors('exclude_admin=0&optioncount=1'); ?>
</ul>
<?php endif;?>

</div>

<?php if( !is_page_template('page-without-comments.php' ) && !is_page_template('authors.php' ) && 'open' == $post->comment_status) : ?>
<?php 
//comments_template();
?>
<?php endif;

if( is_page() ) edit_post_link( __('Edit this page', 'zenlite') );