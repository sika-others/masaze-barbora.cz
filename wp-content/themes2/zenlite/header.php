<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/1">
<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>" />

<meta name="designer" content="esmi@quirm.net" />

<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/print.css" media="print" />

<!--[if IE]>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/ie.css" media="screen" type="text/css" />
<![endif]-->
<!--[if lte IE 7]>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/ie7.css" media="screen" type="text/css" />
<script src="<?php get_template_directory_uri(); ?>/library/focus.js" type="text/javascript"></script>
<![endif]-->

<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php wp_get_archives('type=monthly&format=link'); ?>

<title><?php bloginfo('name');
if(is_search()) {
	if(trim(get_search_query()) != '') printf( __(" - Search Results for '%1$s'", 'zenlite'), trim(get_search_query()) );
	else {bloginfo('name'); _e(' - No search query entered!', 'zenlite');}
}
elseif (is_category() || is_author()) wp_title('-',true);
elseif(is_tag()) printf( __(' - Entries tagged with %1$s ', 'zenlite') , wp_title('', false) );
elseif(is_archive() ) printf( __(' - Archives for %1$s ', 'zenlite') , wp_title('', false) );
elseif(is_404())  _e(' - Page not found!', 'zenlite');
elseif (have_posts()) wp_title('-',true);
?>
</title>

<?php if(is_singular()) wp_enqueue_script( 'comment-reply' );?>
<?php wp_head(); ?>
</head>

<body id="top" <?php body_class(); ?>>

<div id="wrapper">

<ul class="jumplinks">
<li><a href="#content"><?php _e('Jump to Main Content', 'zenlite');?></a></li>
<li><a href="#footer"><?php _e('Jump to Footer', 'zenlite');?></a></li>
</ul>

<div id="header">
<h1><a href="<?php echo home_url( '/' ); ?>" title="<?php _e('Home', 'zenlite');?>"><?php bloginfo('name'); ?></a>
<small><?php bloginfo('description'); ?></small></h1>
</div>

<div id="header-image"></div>

<?php get_sidebar(); ?>

<div id="content">

