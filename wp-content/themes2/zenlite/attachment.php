<?php get_header();

if (have_posts()) : while (have_posts()) : the_post(); ?>

<div <?php post_class(); ?>>

<h2><?php the_title(); ?></h2>
<ul class="meta">
<li><?php edit_post_link(); ?></li>
</ul>

<div class="postcontent">

<p class="download-link"><img class="download-icon alignleft" src="<?php echo wp_mime_type_icon(); ?>" width="46" height="60" alt="" /> <?php printf( __('Download the <a href="%1$s" rel="attachment">%2$s</a> file.', 'zenlite'),  wp_get_attachment_url(), get_the_title() );?><br />

<?php printf( __('Published on %1$s at %2$s', 'zenlite' ), get_the_time(get_option('date_format')), get_the_time(get_option('time_format')) );
if( get_the_modified_time() != get_the_time() ) {
	echo '<br />';
	printf( __('Updated on %1$s at %2$s', 'zenlite' ), get_the_modified_time(get_option('date_format')), get_the_modified_time(get_option('time_format')) );
}?></p>

<?php if ( !empty( $post->post_excerpt ) ) :?>
<div class="attachment-caption">
<?php the_excerpt(); ?>
</div>
<?php endif;?>

<?php if ( !empty( $post->post_content ) ) :?>
<div class="attachment-content">
<?php the_content(); ?>
</div>
<?php endif;?>

</div>

<p class="posted-under"><?php _e('Posted under', 'zenlite');?> <a href="<?php echo get_permalink($post->post_parent); ?>" title="<?php _e('Return to parent page', 'zenlite');?>"><?php echo get_the_title($post->post_parent); ?></a></p>

<?php endwhile; ?><?php endif; ?>
</div>

<?php get_footer();