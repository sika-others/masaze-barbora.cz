ZENLITE THEME

THEME OPTIONS

Use the top navigation menu to display links to: 
	You can chose whether to display Pages or Categories in the top menu. The menu will use dropdowns for child pages or categories. Thiis option will not apply if you use a custom menu via Appearance>Menus.

Display an author link on posts:
	Hide the "Author: [name]" line at the bottom of each post.
	
Display allowed tags on comment form:
	Hide the "You may use these XHTML tags and attribute" lines at the bottom of the comment form on Posts and Pages.
	
FEATURED IMAGES & EXCERPTS
ZenLite supports post thumbnails (featured images). Where a post has a featured image, the image will be displayed on all post listing pages along with an automatic post excerpt (currently set to the WordPress default of 55 words). If a post does not have a featured image, the post content will be used. To shorten this content on all post listing pages, use the <!--more--> tag in your posts.
	

CHILD THEMES
This theme has been developed to support the creation of child themes. Wherever possible, do not customise this theme but create a child theme for your customisations instead. Customising this theme will cause you to lose all of your changes if/when you later upgrade the theme. A child theme will protect your changes but still allow you to upgrade the parent theme.

See <http://codex.wordpress.org/Child_Themes> for further information.

There some child themes available for this theme. See <http://quirm.net/themes/> for a full list. Bespoke/customised child themes can also be developed  for a moderate fee.

SUPPORT
Free support is available at <http://forum.quirm.net/>. You can also find free support at <http://wordpress.org/support/> but, due to the high traffic on the WordPress forums, I cannot guarantee that I will notice or answer your requests for assistance.

Feedback & feature requests are also welcome at <http://forum.quirm.net/>.