<?php
/* ZENLITE POST FORMATS GUIDE
*/
?>
<div id="zenlite-readme">

<h3><?php _e('Post Formats Documentation', 'zenlite');?></h3>

<h4><?php _e('Aside', 'zenlite');?></h4>
<?php _e("Similar to a Facebook note update, these posts will not display a title - although a title will be made available for search engines etc. If a featured image has been chosen, it will be shown on all archive (post listing) pages along with the post's excerpt. If there is no featured image, a default aside image will be shown.", 'zenlite');?>

<h4><?php _e('Gallery', 'zenlite');?></h4>
<?php _e("A gallery of images. If a featured image has been chosen, it will be shown as a link to the single post on all archive (post listing) pages. The gallery itself will only be shown on the single post view. If there is no featured image, a default gallery image will be used instead.", 'zenlite');?>

<h4><?php _e('Link', 'zenlite');?></h4>
<?php _e("A single link to another site. The link's text will be used as the post title along with a small link icon to indicate that it leads to an external page. In the single post view, the link's url (address) will also be shown. No other post content will be displayed.,", 'zenlite');?>

<h4><?php _e('Image', 'zenlite');?></h4>
<?php _e("A post containing a single attached image. If a featured image has been chosen, it will be shown as a link to the single post on all archive (post listing) pages. The gallery itself will only be shown on the single post view. If there is no featured image, a default image icon will be used instead.", 'zenlite');?>

<h4><?php _e('Quote', 'zenlite');?></h4>
<?php _e("A single quotation within <code>blockquote</code> tags.The quotation.post content will be displayed and styled on both archive and single post pages.", 'zenlite');?>

<h4><?php _e('Status', 'zenlite');?></h4>
<?php _e("Similar to a Twitter status update, these posts will not display a title - although a title will be made available for search engines etc. If a featured image has been chosen, it will be shown on all archive (post listing) pages along with the post's excerpt. If there is no featured image, a default status image will be shown.", 'zenlite');?>

<h4><?php _e('Video', 'zenlite');?></h4>
<?php _e("A post containing 1 or more videos. The video itself and any post content will only be shown on the single post view. This format does not support the display of featured images but a default video icon will be displayed on all archive pages.", 'zenlite');?>

<h4><?php _e('Audio', 'zenlite');?></h4>
<?php _e("A post containing 1 or more audio files. The audio file and any post content will only be shown on the single post view. This format does not support the display of featured images but a default audio icon will displayed on all archive pages.", 'zenlite');?>

<h4><?php _e('Chat', 'zenlite');?></h4>
<?php _e("A styled chat transcript. This format does not support the display of featured images but styling will be displayed on both archive and single post pages", 'zenlite');?>

<h4><?php _e('Standard', 'zenlite');?></h4>
<?php _e("A standard wordPress post.", 'zenlite');?>
