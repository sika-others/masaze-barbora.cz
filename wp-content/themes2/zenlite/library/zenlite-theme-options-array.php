<?php

// ZenLite theme options

 $zenlite_all_theme_options = array(
	'menu_type' => array(
		'type' => 'radio',
		'default' => 'pages',
		'options' => array(
			'1' => array(
				'value' => 'pages',
				'label' => __( 'Pages', 'zenlite' )
			),
			'2' => array(
				'value' => 'cats',
				'label' => __( 'Categories', 'zenlite' )
			)
		)
	),
	'header_text' => array(
		'type' => 'radio',
		'default' => 'yes',
		'options' => array(
			'1' => array(
				'value' => 'yes',
				'label' => __( 'Yes', 'zenlite' )
			),
			'2' => array(
				'value' => 'no',
				'label' => __( 'No', 'zenlite' )
			)
		)
	),
	'pagetree' => array(
		'type' => 'radio',
		'default' => 'yes',
		'options' => array(
			'1' => array(
				'value' => 'yes',
				'label' => __( 'Yes', 'zenlite' )
			),
			'2' => array(
				'value' => 'no',
				'label' => __( 'No', 'zenlite' )
			)
		)
	),
	'notitle_display' => array(
		'type' => 'radio',
		'default' => 'yes',
		'options' => array(
			'1' => array(
				'value' => 'yes',
				'label' => __( 'Yes', 'zenlite' )
			),
			'2' => array(
				'value' => 'no',
				'label' => __( 'No', 'zenlite' )
			)
		)
	),
	'author_display' => array(
		'type' => 'radio',
		'default' => 'yes',
		'options' => array(
			'1' => array(
				'value' => 'yes',
				'label' => __( 'Yes', 'zenlite' )
			),
			'2' => array(
				'value' => 'no',
				'label' => __( 'No', 'zenlite' )
			)
		)
	),
	'kses_display' => array(
		'type' => 'radio',
		'default' => 'yes',
		'options' => array(
			'1' => array(
				'value' => 'yes',
				'label' => __( 'Yes', 'zenlite' )
			),
			'2' => array(
				'value' => 'no',
				'label' => __( 'No', 'zenlite' )
			)
		)
	)
);
