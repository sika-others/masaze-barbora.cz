<?php
/*
Template Name: List Authors
*/

get_header();
if (have_posts()) : while (have_posts()) : the_post(); ?>

<div <?php post_class(); ?>>
<?php get_template_part('format', 'page');?>
</div>

<?php endwhile; endif;
get_footer();