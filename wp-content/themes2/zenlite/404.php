<?php ob_start();
header("HTTP/1.1 404 Not Found");
header("Status: 404 Not Found");
get_header(); ?>

<div <?php post_class('page'); ?>>
<h2 class="post-title"><?php _e('Page Not Found', 'zenlite');?></h2>

<div class="postcontent">
<p><?php _e("Uh oh! I can't seem to find the file you asked for.", 'zenlite');?></p>

<p><?php _e('Perhaps you:', 'zenlite');?></p>

<ul>
<li><?php _e('tried to access a post or archive that has been removed', 'zenlite');?></li>
<li><?php _e('followed a bad link', 'zenlite');?></li>
<li><?php _e('mis-typed something', 'zenlite');?></li>
</ul>

</div>

<?php
if( is_active_sidebar( 'widget-404' ) ) :?><div class="widget-area"><?php endif;
if (!dynamic_sidebar( 'widget-404' ) ) : ?>
<p><?php _e("Try using the Search option to find what you're looking for.", 'zenlite');?></p>
<?php get_search_form();
endif;
if( is_active_sidebar( 'widget-404' ) ) :?></div><?php endif;?>

</div>

<?php get_footer();