<?php

if ( ! function_exists( 'zenlite_setup' ) ):
function zenlite_setup() {

	// Load language files
	load_theme_textdomain( 'zenlite', TEMPLATEPATH . '/langs' );
	$locale = get_locale();
	$locale_file = TEMPLATEPATH . "/langs/$locale.php";
	if ( is_readable( $locale_file ) ) require_once( $locale_file );

	// set content width
	if ( ! isset( $content_width ) ) $content_width = 980;

	// remove WP version number - security by obscurity
	remove_action ('wp_head', 'wp_generator');

	// Add post thumbnail & feed link support
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-formats', array( 'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video' ) );
	add_image_size( 'post_format', 100, 100);

	// Add editor style
	add_editor_style();

	// Get the post thumbnail dimensions from Settings/Media
	define('THUMB_WIDTH', get_option('thumbnail_size_w'));
	define('THUMB_HEIGHT', get_option('thumbnail_size_h'));
	set_post_thumbnail_size( THUMB_WIDTH, THUMB_HEIGHT ); // box (proportional) resize mode

	// Add support for custom background
	add_custom_background();

	// Header & Background customisation/
	define('HEADER_IMAGE_WIDTH', apply_filters( 'zenlite_header_image_width',1000 ) );
	define('HEADER_IMAGE_HEIGHT', apply_filters( 'zenlite_header_image_height', 150 ) );
	define('HEADER_IMAGE', get_stylesheet_directory_uri() . '/images/banner.jpg');
	define('HEADER_TEXTCOLOR', apply_filters( 'zenlite_header_color', '606060' ) );
	define ('BACKGROUND_COLOR', apply_filters( 'zenlite_background_color', 'fff' ) );
	define ('BACKGROUND_IMAGE', apply_filters( 'zenlite_background_image', '' ) );

	// Load banner selection images into UI
	register_default_headers( array(
		'one' => array(
			'url' => get_stylesheet_directory_uri() . '/images/headers/header1.jpg',
			'thumbnail_url' => get_stylesheet_directory_uri() . '/images/headers/header1-thumbnail.jpg',
			'description' => __( 'Header 1', 'zenlite' )
		),
		'two' => array(
			'url' => get_stylesheet_directory_uri() . '/images/headers/header2.jpg',
			'thumbnail_url' => get_stylesheet_directory_uri() . '/images/headers/header2-thumbnail.jpg',
			'description' => __( 'Header 2', 'zenlite' )
		),
		'three' => array(
			'url' => get_stylesheet_directory_uri() . '/images/headers/header3.jpg',
			'thumbnail_url' => get_stylesheet_directory_uri() . '/images/headers/header3-thumbnail.jpg',
			'description' => __( 'Header 3', 'zenlite' )
		),
		'four' => array(
			'url' => get_stylesheet_directory_uri() . '/images/headers/header4.jpg',
			'thumbnail_url' => get_stylesheet_directory_uri() . '/images/headers/header4-thumbnail.jpg',
			'description' => __( 'Header 4', 'zenlite' )
		)
	) );

}
endif;
add_action( 'after_setup_theme', 'zenlite_setup' );

// Load custom theme options
require_once(get_template_directory() . '/library/theme-options.php');
$zenlite_options = get_option('zenlite_theme_options');

// Register sidebar
if ( !function_exists( 'zenlite_widgets_init' )) :
function zenlite_widgets_init() {
	register_sidebar(array(
		'name'=> __('Main menu', 'zenlite'),
		'id' => 'main-menu',
		'description' => __('Main horizontal menu. Recommended use: Pages or Categories.','zenlite'),
		'before_widget' => '<div id="%1$s" class="top-menu widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="widgettitle offset">',
		'after_title' => '</h2>',
	));
	register_sidebar(array(
		'name'=> __('Footer', 'zenlite'),
		'id' => 'widget-footer',
		'description' => __('Displayed in the footer area of every page. Just about any widget will work here.','zenlite'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=> __('404 Page', 'zenlite'),
		'id' => 'widget-404',
		'description' => __('Displayed in the Not Found  (404) Page only. Just about any widget will work here.','zenlite'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
}
endif;
add_action( 'widgets_init', 'zenlite_widgets_init' );

// Hide header text
if( !function_exists ('zenlite_hide_header_text') ) :
function zenlite_hide_header_text() {
	global $zenlite_options;
	if( isset( $zenlite_options['header_text'] ) &&  $zenlite_options['header_text'] == 'no' ) echo '<style type = "text/css">
	#header h1 {position:absolute;top:-9999px;left:-9999px;}
	</style>';
}
endif;
add_action( 'wp_head', 'zenlite_hide_header_text' );

// add custom menu support
if ( function_exists( 'register_nav_menu' ) && !function_exists( 'register_theme_menus' )) :
function register_zenlite_menus() {
	register_nav_menu( 'primary', __( 'Primary', 'zenlite' ) );
}
endif;
add_action( 'init', 'register_zenlite_menus' );

// Add thickbox to public-facing attachment pages only
if ( !function_exists( 'zenlite_thickbox_init' ) ) :
function zenlite_thickbox_init() {
	if (is_attachment() || has_post_format('image') ) add_thickbox();
}
endif;
add_action('template_redirect', 'zenlite_thickbox_init');

if ( !function_exists( 'zenlite_thickbox_style' ) ) :
function zenlite_thickbox_style() {
	$css_file = get_stylesheet_directory() . '/thickbox.css';
	$css_url = get_stylesheet_directory_uri() . '/thickbox.css';
	if ( file_exists($css_file) && ( is_attachment() || has_post_format('image')) ) {
		wp_register_style('zenlite-thickbox-style', $css_url, '', '', 'screen');
		wp_enqueue_style('zenlite-thickbox-style');
	}
}
endif;
add_action('wp_print_styles', 'zenlite_thickbox_style');

// Set up a proper &hellip;for post extracts
if( !function_exists ('zenlite_proper_hellip') ) :
function zenlite_proper_hellip($more) {
	return '&hellip;';
}
endif;
add_filter('excerpt_more', 'zenlite_proper_hellip');

// Remove gallery css
if( !function_exists ('zenlite_remove_gallery_css') ) :
function zenlite_remove_gallery_css( $css ) {
	return preg_replace( "#<style type='text/css'>(.*?)</style>#s", '', $css );
}
endif;
add_filter( 'gallery_style', 'zenlite_remove_gallery_css' );

// Correct image path issue in thickbox
if( !function_exists ('zenlite_load_tb_fix') ) :
function zenlite_load_tb_fix() {
	if( is_attachment() ) ?><script type="text/javascript">tb_pathToImage = "'<?php echo home_url();?>/wp-includes/js/thickbox/loadingAnimation.gif";tb_closeImage = "<?php echo home_url();?>/wp-includes/js/thickbox/tb-close.png";</script>
	<?php
}
endif;
add_action('wp_footer', 'zenlite_load_tb_fix');

// Amend post password form
if( !function_exists ('zenlite_password_form') ) :
function zenlite_password_form() {
	global $post;
	$label = 'pwbox-'.(empty($post->ID) ? rand() : $post->ID);
	$output = '<div class="password-form">
	<p class="protected-text">' . __('This post is password protected. To view it, please enter your password below:', 'zenlite') . '</p>
	<form action="' . get_option('siteurl') . '/wp-pass.php" method="post">
	<p><label for="' . $label . '">' . __('Password:', 'zenlite') . ' </label> <input name="post_password" id="' . $label . '" type="password" size="20" /> <input type="submit" name="Submit" value="' . esc_attr__('Submit', 'zenlite') . '" /></p></form></div>';
	return $output;
}
endif;
add_filter('the_password_form','zenlite_password_form');

// Amend password protected standard excerpt
if( !function_exists ('zenlite_password_excerpt') ) :
function zenlite_password_excerpt($output) {
	global $post;
	if ( post_password_required($post) ) $output = '<p class="pwd-protected">' . __('This is a password protected entry.', 'zenlite') . '</p>';
	return $output;
}
endif;
add_filter('the_excerpt','zenlite_password_excerpt');

// Auto-generate post/page titles
if ( !function_exists( 'zenlite_autogenerate_title' ) ) :
function zenlite_autogenerate_title( $title ) {
	global $zenlite_options;
	if ( !$title && ( !isset($zenlite_options['notitle_display']) || $zenlite_options['notitle_display'] == 'yes' ) ) $title = __('No Title', 'zenlite');
	return $title;
}
endif;
add_filter( 'the_title', 'zenlite_autogenerate_title' );

// Enable home link in wp_page_menu
if ( !function_exists( 'zenlite_show_home' ) ) :
function zenlite_show_home( $args ) {
	$args['show_home'] = true;
	return $args;
}
endif;
add_filter( 'wp_page_menu_args', 'zenlite_show_home' );

// Customise menu class in wp_page_menu
if ( !function_exists( 'zenlite_primary_menu_class' ) ) :
function zenlite_primary_menu_class( $args ) {
	$args['menu_class'] = 'top-menu';
	return $args;
}
endif;
add_filter( 'wp_page_menu_args', 'zenlite_primary_menu_class' );

// Extract first occurance of text from a string
if( !function_exists ('zenlite_extract_from_string') ) :
function zenlite_extract_from_string($start, $end, $tring) {
	$tring = stristr($tring, $start);
	$trimmed = stristr($tring, $end);
	return substr($tring, strlen($start), -strlen($trimmed));
}
endif;

// Construct an author posts link
if( !function_exists ('zenlite_author_display') ) :
function zenlite_author_display($before = '', $after = '') {
	global $zenlite_options, $post;
	$zenlite_author_string = '';
	if( !isset( $zenlite_options['author_display'] ) || $zenlite_options['author_display'] == 'yes')  $zenlite_author_string = $before . '<a class="author" href="' . get_author_posts_url($post->post_author) . '">' . get_the_author() . '</a>' . $after;
	return $zenlite_author_string;
}
endif;

// Called in sidebar.php for Categories if no custom menu is defined
if ( !function_exists( 'zenlite_callback_cats' ) ) :
function zenlite_callback_cats() {
	?>
	<ul>
	<li<?php if(is_front_page() || is_home()) echo ' class="current_page_item"';?>><a href="<?php echo home_url(); ?>"><?php _e('Home', 'zenlite');?></a></li>
	<?php wp_list_categories('title_li=');?>
	</ul>
	<?php
}
endif;

// Amend comment form fields
if ( !function_exists( 'zenlite_comment_fields' ) ) :
	function zenlite_comment_fields($fields) {
		$commenter = wp_get_current_commenter();
		$req = get_option('require_name_email');
		if( $req ) $reqd = '<span class="required">' . __('*', 'zenlite') . '</span>';

		$fields['author'] = '<p class="comment-form-author"><label class="text" for="author">' . __( 'Name', 'zenlite' ) . $reqd . '</label><input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" /></p>';

		$fields['email'] = '<p class="comment-form-email"><label class="text" for="email">' . __( 'Email', 'zenlite' ) . $reqd . '</label><input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" />' . '</p>';

		$fields['url']   = '<p class="comment-form-url"><label class="text" for="url">' . __( 'Website', 'zenlite') . '</label>' . '<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p>';

		return $fields;
	}
endif;
add_filter('comment_form_default_fields','zenlite_comment_fields');

// Amend comment form arguments
if ( ! function_exists( 'zenlite_comment_form_args' ) ) :
function zenlite_comment_form_args($user_identity, $post_id, $req) {
	global $zenlite_options;
	$args = array(
		'comment_notes_before' => '<p class="comment-notes"><label for="author">' . ( $req ? __( ' Required fields are marked ', 'zenlite') . '<span class="required">' . __('*', 'zenlite') . '</span><br />' : '' ) . __( 'Your details will be stored in a non-tracking cookie but your email address will <em>never</em> be published or shared.' ) . '</label></p>',

		'must_log_in' => '<p class="must-log-in">' .  sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment. If you choose to login - or register - on this site, a non-tracking cookie will be stored on your computer but your email address will <em>never</em> be published or shared.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',

		'logged_in_as' => '<p class="logged-in-as"><label for="comment">' . sprintf( __( 'Logged in as <a href="%s">%s</a>. <a href="%s" title="Log out of this account">Log out?</a></label></p>' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ),

		'comment_field' => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label><textarea id="comment" name="comment" cols="45" rows="8"></textarea></p>',
		'label_submit'=>'Submit Reply',

		'comment_notes_after' => '<p class="form-allowed-tags"><label for="comment">' . __( 'You may use these <abbr title="eXtensible HyperText Markup Language">XHTML</abbr> tags and attributes:', 'zenlite' ) . ' <code>' .  allowed_tags() . '</code></label></p>'
	);
	if( $zenlite_options['kses_display'] && $zenlite_options['kses_display'] == 'no' ) $args['comment_notes_after'] = '';
	return $args;
}
endif;

// Return page tree
if ( !function_exists( 'zenlite_page_tree' ) ) :
function zenlite_page_tree($this_page) {
	$pagelist = '';
	if( !$this_page->post_parent ) {
		$children = wp_list_pages('title_li=&child_of='.$this_page->ID.'&echo=0');
		if( $children ) {
			$pagelist .= '<li class="current_page_item"><a href="'.  get_page_link($this_page->ID) .'">' . $this_page->post_title . '</a>';
			$pagelist .= '<ul>' . $children . '</ul>';
			$pagelist .= '</li>';
		}
	}
	elseif( $this_page->ancestors ) {
		// get the top ID of this page. Page ids DESC so top level ID is the last one
		$ancestor = end($this_page->ancestors);
		$pagelist .= wp_list_pages('title_li=&include='.$ancestor.'&echo=0');
		$pagelist = str_replace('</li>', '', $pagelist);
		$pagelist .= '<ul>' . wp_list_pages('title_li=&child_of='.$ancestor.'&echo=0') .'</ul></li>';
	}
	return $pagelist;
}
endif;

// output custom header image & text CSS
if ( !function_exists( 'zenlite_custom_header_style' ) ) :
function zenlite_custom_header_style() {
	echo '<style type="text/css" media="screen,print">'."\n";
	if( get_header_image() != '' ) echo '#header-image {background-image:url(' . get_header_image() . ');}';
	if( get_header_textcolor() == 'blank' ) echo '#header h1,#header h1 a {position:absolute;top:-5000px;left:-5000px;}';
	else echo '#header h1,#header h1 a {color:#' . get_header_textcolor() . ';}';
	echo "\n</style>\n\n";
}
endif;

// Style header customisation in the Admin area
if ( !function_exists( 'zenlite_admin_header_style' ) ) :
function zenlite_admin_header_style() {
?>
<style type="text/css">
#headimg {
	height:150px;
	padding:0;
	margin:70px 0 0;
	background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/banner.jpg);
	background-repeat:no-repeat;
	background-position:top left;
	border-top:3px double #aaa;
	border-bottom:3px double #aaa;
}
#headimg h1,#headimg #desc {
	text-align:center;
	font-weight:normal;
	letter-spacing:.02em;
	position:relative;
	top:-70px;
}
#headimg h1 {
	margin:0;
	padding:0 15px;
	font-size:30px;
	line-height:1.2em;
	color:#606060;
}
#headimg #desc {
	display:block;
	margin:0;
	padding:0 0 13px;
	font-size:20px;
	color:#707070;
}
#headimg a {
	text-decoration:none;
}
#headimg a:hover {
	text-decoration:underline;
}
</style>
<?php
}
endif;
add_custom_image_header('zenlite_custom_header_style', 'zenlite_admin_header_style');
