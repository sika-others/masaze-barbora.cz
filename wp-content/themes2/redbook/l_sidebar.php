<!-- begin l_sidebar -->

	<div id="l_sidebar">
	<ul id="l_sidebarwidgeted">
	<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(1) ) : else : ?>
		
	<li id="Pages">
	<h2>Pages</h2>
		<ul>
			<?php wp_list_pages('title_li=&depth=1'); ?>
		</ul>
	</li>
	
	<li id="Recent">
	<h2>Recently Written</h2>
		<ul>
			<?php get_archives('postbypost', 10); ?>
		</ul>
	</li>
		
	<li id="Admin">
	<h2>Admin</h2>
		<ul>
			<?php wp_register(); ?>
			<li><?php wp_loginout(); ?></li>
			<li><a href="http://www.wordpress.org/">WordPress</a></li>
			<?php wp_meta(); ?>
			<li><a href="http://validator.w3.org/check?uri=referer">XHTML</a></li>
		</ul>
	</li>
		
		<?php endif; ?>
		</ul>
			
</div>

<!-- end l_sidebar -->