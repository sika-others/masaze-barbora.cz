<!-- begin r_sidebar -->

	<div id="r_sidebar">
	<ul id="r_sidebarwidgeted">
	<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(2) ) : else : ?>

	<li id="Search">	
	<h2>Search</h2>
	   <form id="searchform" method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
	   <input type="text" value="Type words and hit enter..." name="s" id="s" onfocus="if (this.value == 'Type words and hit enter...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Type words and hit enter...';}" /></form>
	</li>
	
	<li id="Categories">
	<h2>Categories</h2>
		<ul>
			<?php wp_list_cats('sort_column=name'); ?>
		</ul>
	</li>
		
	<li id="Archives">
	<h2>Archives</h2>
		<ul>
			<?php wp_get_archives('type=monthly'); ?>
		</ul>
	</li>

	<li id="Blogroll">
	<h2>Blogroll</h2>
		<ul>
			<?php get_links(-1, '<li>', '</li>', ' - '); ?>
		</ul>
	</li>
		
	<?php endif; ?>
	</ul>
			
</div>

<!-- end r_sidebar -->