<?php get_header(); ?>

<div id="content">

<?php include(TEMPLATEPATH."/l_sidebar.php");?>

	<div id="contentleft">
	
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<h1><?php the_title(); ?></h1>
		
		<div class="postmeta">
			<p><b><?php the_time('l'); ?></b>&nbsp;<?php the_time('F j, Y'); ?>&nbsp;<?php edit_post_link('(Edit)', '', ''); ?></p>
		</div>
	
		<?php the_content(__('Read more'));?><div style="clear:both;"></div><small>Filed Under <?php the_category(', ') ?></small>
	 			
		<!--
		<?php trackback_rdf(); ?>
		-->
		
		<?php endwhile; else: ?>
		
		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p><?php endif; ?>
		<p><?php posts_nav_link(' &#8212; ', __('&laquo; Previous Page'), __('Next Page &raquo;')); ?></p>
	
		<h4>Comments</h4>
		<?php comments_template(); // Get wp-comments.php template ?>
	
	</div>
	
<?php include(TEMPLATEPATH."/r_sidebar.php");?>

</div>

<!-- The main column ends  -->

<?php get_footer(); ?>